// stateless functional component

class IndecisionApp extends React.Component {
  constructor(props) {
    super(props);
    this.handleDeleteOptions = this.handleDeleteOptions.bind(this);
    this.handlePick = this.handlePick.bind(this);
    this.handleAddOption = this.handleAddOption.bind(this);
    this.handleDeleteOption = this.handleDeleteOption.bind(this);
    this.state = {
      options: props.options
    };
  }
  componentDidMount() {
    try {
      const json = localStorage.getItem('options');
      const options = JSON.parse(json);

      if (options) {
        this.setState(() => ({ options }));
      }
    } catch (e) {
      // Do nothing at all if JSON is invalid
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.options.length !== this.state.options.length) {
      const json = JSON.stringify(this.state.options);
      localStorage.setItem('options', json);
      console.log('remove data');
    }
  }
  componentWillUnmount() {
    console.log('componentWillUnmount');
  }
  handleDeleteOptions() {
    /**
     * if you want to return an object wrap it in ( )
     * const num = () => ({});
     * if not it will be treated as normal function
     */
    this.setState(() => ({ options: [] }));
  }
  handleDeleteOption(optionToRemvoe) {
    this.setState(prevState => ({
      options: prevState.options.filter(option => optionToRemvoe !== option)
    }));
  }
  handlePick() {
    const randomNum = Math.floor(Math.random() * this.state.options.length);
    const option = this.state.options[randomNum];
    alert(option);
  }
  handleAddOption(option) {
    if (!option) {
      return 'Enter valid value to add item';
    } else if (this.state.options.indexOf(option) > -1) {
      return 'This option already exists';
    }

    /**
     * ex. change state below
     */
    this.setState(prevState => ({ options: prevState.options.concat(option) }));
  }
  render() {
    // const title = 'Indecision';
    const subtitle = 'Put your life in the hands of a computer';

    return React.createElement(
      'div',
      null,
      React.createElement(Header, { subtitle: subtitle }),
      React.createElement(Action, {
        hasOptions: this.state.options.length > 0,
        handlePick: this.handlePick
      }),
      React.createElement(Options, {
        options: this.state.options,
        handleDeleteOptions: this.handleDeleteOptions,
        handleDeleteOption: this.handleDeleteOption
      }),
      React.createElement(AddOption, {
        handleAddOption: this.handleAddOption
      })
    );
  }
}

IndecisionApp.defaultProps = {
  options: []
};

const Header = props => {
  return React.createElement(
    'div',
    null,
    React.createElement(
      'h1',
      null,
      props.title
    ),
    props.subtitle && React.createElement(
      'h2',
      null,
      props.subtitle
    )
  );
};

Header.defaultProps = {
  title: 'Indecision'
};

const Action = props => {
  return React.createElement(
    'div',
    null,
    React.createElement(
      'button',
      {
        onClick: props.handlePick,
        disabled: !props.hasOptions
      },
      'What should I do?'
    )
  );
};

const Options = props => {
  return React.createElement(
    'div',
    null,
    React.createElement(
      'button',
      { onClick: props.handleDeleteOptions },
      'Remove All'
    ),
    props.options.length === 0 && React.createElement(
      'p',
      null,
      'Please add an option to get started!'
    ),
    props.options.map(option => React.createElement(Option, {
      key: option,
      optionText: option,
      handleDeleteOption: props.handleDeleteOption
    }))
  );
};

const Option = props => {
  return React.createElement(
    'div',
    null,
    props.optionText,
    React.createElement(
      'button',
      {
        onClick: e => {
          props.handleDeleteOption(props.optionText);
        }
      },
      'remove'
    )
  );
};

class AddOption extends React.Component {
  constructor(props) {
    super(props);
    this.handleAddOption = this.handleAddOption.bind(this);
    this.state = {
      error: undefined
    };
  }
  handleAddOption(e) {
    e.preventDefault();

    const option = e.target.elements.option.value.trim();
    const error = this.props.handleAddOption(option);

    /**
     * ex. change state below
     * this.setState(() => { return { error }; });
     */
    this.setState(() => ({ error }));

    /**
     * When we add valid data, an input get wiped
     */
    if (!error) {
      e.target.elements.option.value = '';
    }
  }
  render() {
    return React.createElement(
      'div',
      null,
      this.state.error && React.createElement(
        'p',
        null,
        this.state.error
      ),
      React.createElement(
        'form',
        { onSubmit: this.handleAddOption },
        React.createElement('input', { type: 'text', name: 'option' }),
        React.createElement(
          'button',
          null,
          'Add Option'
        )
      )
    );
  }
}

/**
const User = () => {
  return (
      <div>
        <p>Name: </p>
        <p>Age: </p>
      </div>
  )
};
 **/

ReactDOM.render(React.createElement(IndecisionApp, { options: ['Devils den', 'Second District'] }), document.getElementById('app'));
