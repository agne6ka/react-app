## INSTALL YARN AND SERVER
`yarn global add live-server`

`npm install -g live-server`

`yarn add babel-preset-react@6.24.1  babel-preset-env@1.5.2`

## BABEL
`babel src/app.js --out-file=public/scripts/app.js --preset=evn, react --watch` - via babel => not working (old cmd)

`./node_modules/.bin/babel src/app.js --out-file=public/scripts/app.js --preset=env, react --watch`

`babel-node src/app.js --out-file public/scripts/app.js --presets es2015, react` - via babel-cli => not working

Working one

`./node_modules/.bin/babel src/app.js --out-file public/scripts/app.js --presets react --watch`

Rund form playground

`./node_modules/.bin/babel src/playground/counter-example.js --out-file public/scripts/app.js --presets react --watch`

## RUN SERVER
live-server public

## GLOBAL VAR IN DEV TOOLS
`$r` - access advanced options in web console

## COMPONENT TYPES
[Intro to React components](https://code.tutsplus.com/tutorials/stateful-vs-stateless-functional-components-in-react--cms-29541)
* **stateless functional component**
* **stateful functional component**
* **class based components**
* **functional components**

##  LIFECYCLE METHODS
##### Class based components have access to them.
[React component lifecycle methods doc](https://reactjs.org/docs/react-component.html#componentdidmount)
* **componentDidMount** - after a component is mounted
* **componentDidUpdate** - when the props or state value change. Inside the method w can access this.props and this.state. Also can access few arguments with previous props and state (prevProps, prevState).
* **componentWillUnmount** - fires before the component go away.